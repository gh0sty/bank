# GNU Taler Demo Bank

This code implements a bank Web portal that tightly integrates
with the GNU Taler payment system.  The bank it primarily meant
be used as part of a demonstrator for the Taler system, but
can also be used in standalone deployments for a regional/local
currency.

## Deprecation Notice

This project is being replaced with https://git.taler.net/libeufin.git
No more feature will be worked in this project so hack at your own risk.

## Installation

### PyPI

To install the bank without building it from source, run
```
$ pip3 install --user talerbank
```
to get the latest version published in the Python Package Index (PyPI).


### Prerequisites

To build the bank, you need:
* ``python>=3.8``
* ``pip3``
* ``poetry``
  * Either install with ``pip3 install --user poetry``, your distribution's package manager,
  or as recommended in the [poetry documentation](https://python-poetry.org/docs/#installation).


### GNU-Style Installation

If you are building from the git repository, first run ``./boostrap``.

Then run
```
$ ./configure
$ make install
```
to install the bank for the current user.


### Custom Installation

To install with custom options, first build the wheel for the bank
and then install via ``pip3``:
```
$ ./configure
$ make wheel
$ pip3 $CUSTOM_OPTIONS ./dist/talerbank-$VERSION-py3-none-any.whl
```


## Internationalization

After new strings to translate enter the bank's text, run the
following command in order to expand the current PO files:

```
$ make i18n-strings
```

It is then required to put the extended PO files under versioning.

The following command creates *new* languages to be translated.  In
particular, it produces the PO file for the language being added.

```
$ python3 manage.py makemessages -l $LANG
```

However, you should probably use https://weblate.taler.net/ to add
new languages instead.


## Configuring the Bank

The bank obeys to the INI syntax for configuration files.
When launched, the bank will by default look for a configuration
file located at ~/.config/taler.conf.  To override this behaviour,
give the -c option when launching the bank.

The following configuration instance makes the bank serve over
HTTP, at port 5882.

```
[taler]
currency = LOCALKUDOS

[bank]
serve = http
http_port = 5882
database = postgres:///talerlocal
max_debt = LOCALKUDOS:500.0
max_debt_bank = LOCALKUDOS:1000000000.0
allow_registrations = YES
base_url = http://localhost:5882/
suggested_exchange = http://localhost:5884/
```


## Launching the Bank

```
$ taler-bank-manage serve
```

## Running Tests

From the repository's top directory:

$ make check


## How to Force Migrations

https://simpleisbetterthancomplex.com/tutorial/2016/07/26/how-to-reset-migrations.html
