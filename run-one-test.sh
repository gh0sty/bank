#!/usr/bin/env bash

# See Bug #5850 for some tests that are currently skipped

if [[ -z $1 ]]; then
    echo "Usage: ./run-one-test.sh 'TestClassName'"
    exit 0
fi

export TALER_CONFIG_FILE="talerbank/app/testconfigs/bank-check.conf"
exec ./manage.py test --no-input talerbank.app.tests.$1
