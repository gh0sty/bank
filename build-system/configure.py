# This configure.py file is places in the public domain.

# Configure the build directory.
# This file is invoked by './configure' and should usually not be invoked
# manually.

import talerbuildconfig as tbc
import sys
import shutil

if getattr(tbc, "serialversion", 0) < 2:
    print("talerbuildconfig outdated, please update the build-common submodule and/or bootstrap")
    sys.exit(1)

b = tbc.BuildConfig()

# Declare dependencies
b.add_tool(tbc.PosixTool("find"))
b.add_tool(tbc.PosixTool("awk"))
b.add_tool(tbc.GenericTool("pip3", version_arg="--version"))
b.add_tool(tbc.GenericTool("poetry", version_arg="-V"))

b.run()

print("copying Makefile")
shutil.copyfile("build-system/Makefile", "Makefile")
