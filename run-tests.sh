#!/usr/bin/env bash

# See Bug #5850 for some tests that are currently skipped

export TALER_CONFIG_FILE="talerbank/app/testconfigs/bank-check.conf"
exec ./manage.py test --no-input talerbank.app.tests "$@"
