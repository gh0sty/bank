import logging

FMT = "%(asctime)-15s %(module)s %(levelname)s %(message)s"
logging.basicConfig(format=FMT, level=logging.WARNING)
